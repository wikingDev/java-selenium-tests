import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class AddProcessNegativeTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongProcessesNames() {
        return new Object[][]{
                {"ab", "The field Name must be a string with a minimum length of 3 and a maximum length of 30."},
                {"", "The Name field is required."},
                {"abababababababababababababababa", "The field Name must be a string with a minimum length of 3 and a maximum length of 30."}
        };
    }

    @Test(dataProvider = "getWrongProcessesNames")
    public void shouldNotCreateProcessWithIncorrectName(String processName, String expError){
        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToProcesses()
                .addNewProcess()
                .typeName(processName)
                .submitCreateWithFailure()
                .assertProcessNameError(expError)
                .backToList()
                .assertProcessIsNotShown(processName);
    }
}
