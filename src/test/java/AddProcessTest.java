import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class AddProcessTest extends SeleniumBaseTest {

    @Test
    public void shouldCreateProcessWithRequiredFields() {
        String processName = UUID.randomUUID().toString().substring(0, 10);
        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToProcesses()
                .addNewProcess()
                .typeName(processName)
                .clickCreateBtn()
                .assertProcess(processName, "", "");
    }

    @Test
    public void shouldCreateProcessWithAllFields() {
        String processName = UUID.randomUUID().toString().substring(0, 10);
        String processDescription = UUID.randomUUID().toString().substring(0, 10);
        String processNotes = UUID.randomUUID().toString().substring(0, 10);
        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToProcesses()
                .addNewProcess()
                .typeName(processName)
                .typeDescription(processDescription)
                .typeNotes(processNotes)
                .clickCreateBtn()
                .assertProcess(processName, processDescription, processNotes);

    }
}
