import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class RegisterNegativeTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void shouldNotRegisterWithIncorrectEmail(String wrongEmail) {
        new LoginPage(driver)
                .clickRegister()
                .typeEmail(wrongEmail)
                .typePassword("Test1!")
                .typeConfirmPassword("Test1!")
                .submitRegisterWithFailure()
                .assertRegisterErrorIsShown("The Email field is not a valid e-mail address.");
    }
    @DataProvider
    public Object[][] wrongPasswords() {
        return new Object[][]{
                {"test", "The Password must be at least 6 and at max 100 characters long."},
                {"test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Testing1", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "wrongPasswords")
    public void shouldNotRegisterWithIncorrectPassword(String wrongPassword, String expErrors) {
        new LoginPage(driver)
                .clickRegister()
                .typeEmail("test@test.com")
                .typePassword(wrongPassword)
                .typeConfirmPassword(wrongPassword)
                .submitRegisterWithFailure()
                .assertRegisterErrorIsShown(expErrors);
    }
}
