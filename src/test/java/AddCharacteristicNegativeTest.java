import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class AddCharacteristicNegativeTest extends SeleniumBaseTest {

    @Test
    public void shouldNotCreateCharacteristicWithoutProject() {
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToCharacteristics()
                .addNewCharacteristic()
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreateWithFailure()
                .assertProcessError("The value 'Select process' is not valid for ProjectId.")
                .backToList()
                .assertCharacteristicIsNotShown(characteristicName);
    }
}

