import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class RegisterPositiveTest extends SeleniumBaseTest{
    @Test
    public void shouldRegister(){
        String email = UUID.randomUUID().toString().substring(0, 10) + "@test.com";
        new LoginPage(driver)
                .clickRegister()
                .typeEmail(email)
                .typePassword("Test1!")
                .typeConfirmPassword("Test1!")
                .submitRegister()
                .assertWelcomeElementIsShown();
    }
}
