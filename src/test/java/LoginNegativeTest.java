import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginNegativeTest extends SeleniumBaseTest {

    @Test
    public void shouldNotLoginWithNotRegisteredUser() {
        new LoginPage(driver)
                .typeEmail("test@test.co")
                .typePassword("Test1!")
                .submitLoginWithFailure()
                .assertLoginErrorIsShown("Invalid login attempt.");
    }

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void shouldNotLoginWithIncorrectEmail(String wrongEmail) {
        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .submitLoginWithFailure()
                .assertLoginErrorIsShown("The Email field is not a valid e-mail address.");
    }
}
