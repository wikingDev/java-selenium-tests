import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class ReportTest extends SeleniumBaseTest{

    @Test
    public void shouldReportCorrectCalculations() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";
        String sampleName = "Test sample";
        String results = "8.0;9.0";
        String expMean = "8.5000";

        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToCharacteristics()
                .addNewCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate()
                .goToResults(characteristicName)
                .clickAddResults()
                .typeSampleName(sampleName)
                .typeResults(results)
                .submitCreate()
                .backToCharacteristics()
                .goToReport(characteristicName)
                .assertMean(expMean);
    }
}
