import org.testng.annotations.Test;
import pages.LoginPage;

public class MenuTest extends SeleniumBaseTest{
    @Test
    public void shouldMenuRedirectToCorrectPages() {
        new LoginPage(driver)
                .login(config.getApplicationUser(), config.getApplicationPassword())
                .goToProcesses()
                .assertProcessesPageIsShown()
                .goToCharacteristics()
                .assertCharacteristicsPageIsShown()
                .goToDashboard()
                .assertDashboardPageIsShown();
    }
}
