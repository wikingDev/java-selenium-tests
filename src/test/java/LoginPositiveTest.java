import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginPositiveTest extends SeleniumBaseTest{
    @Test
    public void shouldLogin(){
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertWelcomeElementIsShown();
    }
}
