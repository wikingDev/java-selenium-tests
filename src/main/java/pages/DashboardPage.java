package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage{

    public DashboardPage (WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title h2")
    private WebElement headerElm;

    public DashboardPage assertDashboardPageIsShown() {
        Assert.assertEquals(driver.getCurrentUrl(), "http://localhost:4444/");
        Assert.assertTrue(headerElm.isDisplayed());
        Assert.assertEquals(headerElm.getText(), "DEMO PROJECT");
        return this;
    }
}
