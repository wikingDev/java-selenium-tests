package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateProcessPage {
    private WebDriver driver;

    public CreateProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Name")
    private WebElement processNameTxt;

    @FindBy(css = ".btn.btn-success")
    private WebElement createBtn;

    @FindBy(id = "Description")
    private WebElement processDescriptionTxt;

    @FindBy(id = "Notes")
    private WebElement processNotesTxt;

    @FindBy(css = ".field-validation-error[data-valmsg-for=Name]")
    private WebElement nameError;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBtn;

    public CreateProcessPage typeName(String name) {
        processNameTxt.sendKeys(name);
        return this;
    }

    public CreateProcessPage typeDescription(String description) {
        processDescriptionTxt.sendKeys(description);
        return this;
    }

    public CreateProcessPage typeNotes(String notes) {
        processNotesTxt.sendKeys(notes);
        return this;
    }

    public ProcessesPage clickCreateBtn() {
        createBtn.click();
        return new ProcessesPage(driver);
    }

    public CreateProcessPage submitCreateWithFailure() {
        createBtn.click();
        return this;
    }

    public CreateProcessPage assertProcessNameError(String expError) {
        Assert.assertEquals(nameError.getText(), expError);
        return this;
    }

    public ProcessesPage backToList() {
        backToListBtn.click();
        return new ProcessesPage(driver);
    }
}


